package foo.backend;

import java.io.IOException;
import java.io.OutputStream;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import java.util.Arrays;
import java.util.ArrayList;

final class X12Writer {
    private static final int BUF_SIZE = 512;
    private static final Charset LATIN1 = Charset.forName("ISO-8859-1");

    private final ByteBuffer _buf;
    private final Config _cfg;
    private final OutputStream _out;

    X12Writer(Config cfg, OutputStream out) {
        _buf = ByteBuffer.allocate(BUF_SIZE);

        _cfg = cfg;
        _out = out;
    }

    void write(String[][] data) {
        Arrays.stream(data).forEach(this::writeBuffer);
    }

    void write(ArrayList<String[]> data) {
        data.stream().forEach(this::writeBuffer);
    }

    // Append each element of the String-based segment array to a buffer after
    // converting each String element to byte[], adding the element delim between
    // elements, finalizing with the segment delim.
    //
    // TODO Handle X12 segments with composite delims as simple String[] of elements
    //      carries no metadata as to which elements belong to the composite.
    private void writeBuffer(String[] array) {
        _buf.clear();

        int arrLen = array.length;
        for (int i = 0; i < arrLen; i++) {
            if (array[i] != null) {
                _buf.put(array[i].getBytes(LATIN1));
            }
            if (i != arrLen-1) {
                _buf.put(_cfg.getElementDelimiter());
            }
        }
        _buf.put(_cfg.getSegmentDelimiter());

        try {
            _out.write(_buf.array(), 0, _buf.position());
        } catch (IOException ioe) {
            // TODO don't suppress
        }
    }
}
