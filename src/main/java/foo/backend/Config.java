package foo.backend;

final class Config {
    private final byte[] _elemDelim;
    private final byte[] _segDelim;
    private final byte[] _compDelim;

    Config() {
        _elemDelim = new byte[] {0x2A};
        _segDelim = new byte[] {0x0A};
        _compDelim = new byte[] {0x2F};
    }

    Config(byte[] element, byte[] segment, byte[] composite) {
        _elemDelim = element;
        _segDelim = segment;
        _compDelim = composite;
    }

    byte[] getElementDelimiter() {
        return _elemDelim;
    }

    byte[] getSegmentDelimiter() {
        return _segDelim;
    }

    byte[] getCompositeDelimiter() {
        return _compDelim;
    }
}
