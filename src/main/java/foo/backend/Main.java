package foo.backend;

import java.io.IOException;
import java.io.OutputStream;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.ArrayList;

final class Main {

    public static final void main(String[] args) throws IOException {
        if (args.length != 2) {
            usage();
            return;
        }
        String variant = args[0].toLowerCase();
        String outFile = args[1];

        // TODO get values from external source
        Config cfg = new Config(new byte[] {0x2A},
                                new byte[] {0x0A},
                                new byte[] {0x2F});

        switch (variant) {
            case "array":
                try (OutputStream out = Files.newOutputStream(Paths.get(String.format("%s.%s", outFile, variant)))) {
                    writeMultiStringArrayData(cfg, out);
                }
                break;
            case "list":
                try (OutputStream out = Files.newOutputStream(Paths.get(String.format("%s.%s", outFile, variant)))) {
                    writeListOfStringArrayData(cfg, out);
                }
                break;
            default:
                usage();
                return;
        }
    }

    private static void writeMultiStringArrayData(Config cfg, OutputStream out) {
        X12Writer x12 = new X12Writer(cfg, out);

        // TODO get from external source
        String[][] data = {
            {"ISA","00","          ","00","          ","ZZ","TESTTESTTES    ","ZZ","NNNNNN         ","170222","1607","U","00401","000000001","0","T","\u001F"},
            {"GS","D3","AAAAAA","NNNNNN","20170222","1607","1","X","004010"},
            {"ST","567","000000001"},
            {"BC","00","20170125","2109",null,null,"V1"},
            {"N1","C4",null,"10","AAAAAA",null,"FR"},
            {"N1","QB",null,"10","NNNNNN",null,"TO"},
            {"CS","NNNNNNNNDDDDD","1G","Z7Z7","DD","C"},
            {"AMT","BU","0"},
            {"N9","VV","F00000"},
            {"G62","BM","20170112"},
            {"G62","BN","20170123"},
            {"G62","BP","20170129"},
            {"LM","DF"},
            {"LQ","0","XYZ"},
            {"LQ","19","C"},
            {"LQ","10","F"},
            {"N1","PR",null,"M3","8558"},
            {"SE","16","000000001"},
            {"GE","1","1"},
            {"IEA","1","000000001"}
        };

        x12.write(data);
    }

    private static void writeListOfStringArrayData(Config cfg, OutputStream out) {
        X12Writer x12 = new X12Writer(cfg, out);

        // TODO get from external source
        ArrayList<String[]> data = new ArrayList<String[]>();
        data.add(new String[] {"ISA","00","          ","00","          ","ZZ","TESTTESTTES    ","ZZ","NNNNNN         ","170222","1607","U","00401","000000001","0","T","\u001F"});
        data.add(new String[] {"GS","D3","AAAAAA","NNNNNN","20170222","1607","1","X","004010"});
        data.add(new String[] {"ST","567","000000001"});
        data.add(new String[] {"BC","00","20170125","2109",null,null,"V1"});
        data.add(new String[] {"N1","C4",null,"10","AAAAAA",null,"FR"});
        data.add(new String[] {"N1","QB",null,"10","NNNNNN",null,"TO"});
        data.add(new String[] {"CS","NNNNNNNNDDDDD","1G","Z7Z7","DD","C"});
        data.add(new String[] {"AMT","BU","0"});
        data.add(new String[] {"N9","VV","F00000"});
        data.add(new String[] {"G62","BM","20170112"});
        data.add(new String[] {"G62","BN","20170123"});
        data.add(new String[] {"G62","BP","20170129"});
        data.add(new String[] {"LM","DF"});
        data.add(new String[] {"LQ","0","XYZ"});
        data.add(new String[] {"LQ","19","C"});
        data.add(new String[] {"LQ","10","F"});
        data.add(new String[] {"N1","PR",null,"M3","8558"});
        data.add(new String[] {"SE","16","000000001"});
        data.add(new String[] {"GE","1","1"});
        data.add(new String[] {"IEA","1","000000001"});

        x12.write(data);
    }

    private static void usage() {
        System.err.println(
                "USAGE: java -jar backend.jar VARIANT OUTFILE\n" +
                "\n" +
                "where args are:\n" +
                "  VARIANT   data structure to use (array, list)\n" +
                "  OUTFILE   output file basename"
                );
    }
}
