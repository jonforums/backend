# X12 Translator Backend Prototype

## Build and Test

~~~ console
# build an executable jar
$ gradlew jar

# test
$ gradlew test

# build, test, and assemble runnable distribution archives
$ gradlew build
~~~

## Execute

~~~ console
# build and run via gradle
$ gradlew run

# build executable jar, run, and create foo.{array,list} X12 files
$ gradlew jar
$ java -jar build/libs/backend-x.y.z.jar array foo
$ java -jar build/libs/backend-x.y.z.jar list foo

# help
$ java -jar backend-x.y.z.jar
USAGE: java -jar backend.jar VARIANT OUTFILE

where args are:
  VARIANT   data structure to use (array, list)
  OUTFILE   output file basename
~~~
